﻿var InvalidIDException, InvalidPhoneNumberException, InvalidPriceException, InvalidVendorIDException, InvalidVendorNameException;
var InvalidContactNumberException, InvalidEmailException;

InvalidIDException = "Invalid Product ID! Please enter valid Product ID";
InvalidPhoneNumberException = "Invalid Phone Number! Please enter valid Phone Number";
InvalidPriceException = "Invalid Price! Please enter valid Price";
InvalidVendorIDException = "Invalid Vendor ID! Please enter valid Vendor ID";
InvalidVendorNameException = "Invalid Vendor Name! Please enter valid Vendor Name";
InvalidContactNumberException = "Invalid Contact Number! Please enter valid Contact Number";
InvalidEmailException = "Invalid Email ID! Please enter valid Email ID";

var tableHeader = ' <tr> \
						<th>Product ID</th> \
						<th>Name</th> \
						<th>Price</th> \
					</tr>';

var newRowButton = '	<tr> \
						<td colspan="3"> \
							<button class="btn btn-success hoverEffect" id="addProductButton" onclick="insertNewRow()"> \
							<span class="glyphicon glyphicon-plus"></span> Add Product</button> \
						</td> \
					</tr> ';

var newRow = '<tr> \
				<td> <input type="text" class="form-control" placeholder="Product ID" id="productId"> </td> \
				<td> <input type="text" class="form-control" placeholder="Name" id="productName"> </td> \
				<td> <input type="text" class="form-control" placeholder="Price" id="productPrice"> </td> \
			</tr> ';

function init() {
    viewVendors();
}

function viewVendors() {
    var vendorsPanel = document.getElementById("vendorsDiv");

    vendorsPanel.innerHTML = '<table id="vendorDetails" style="margin-top:1%;" class=" table table-striped"> \
			<tr> \
				<th>Vendor ID</th> \
				<th>Name</th> \
				<th>Contact</th> \
				<th>Email ID</th> \
				<th>Operations</th> \
			</tr> \
			</table>';

    var tab = document.getElementById("vendorDetails");

    //remove - test code
    for (i = 0; i < 10; i++) {
        tab.innerHTML += '<tr> \
				<td>'+ (i + 1) + '</td> \
				<td>Vendor '+ (i + 1) + '</td> \
				<td>9871923798</td> \
				<td>email.sample@one.com</td> \
				<td><button class="btn btn-success btn-xs"><span class="glyphicon glyphicon-eye-open"></button></span> \
					<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-edit"></button></span> \
					<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></button></span> \
				</td> \
			</tr>';
    }//remove - test code
}

function addVendor() {
    var vendors = document.getElementById("vendorsDiv");

    vendors.innerHTML = ' \
		<div class="table-responsive"> \
			<table class="table"> \
				<tr> \
					<td>Vendor ID</td> \
					<td><input type="text" class="form-control" placeholder="Vendor ID" id="vendorId"></td> \
				</tr> \
				<tr> \
					<td>Vendor Name</td> \
					<td><input type="text" class="form-control" placeholder="Vendor Name" id="vendorName"></td> \
				</tr> \
				<tr> \
					<td>Contact Number</td> \
					<td><input type="text" class="form-control" placeholder="Contact Number" id="contactNumber"></td> \
				</tr> \
				<tr> \
					<td>Email</td> \
					<td><input type="text" class="form-control" placeholder="Email" id="email"></td> \
				</tr> \
				<tr> \
					<td>Products</td> \
					<td> \
						<div class="table-responsive"> \
							<table class="table" id="productsTable"> \
							</table> \
						</div> \
					</td> \
				</tr> \
			</table> \
		</div> \
		<br/> \
		<button class="btn btn-success hoverEffect"> \
			<span class="glyphicon glyphicon-save"></span> Add Vendor \
		</button>';

    document.getElementById("vendorId").focus();
    addProducts();
}

function addProducts() {
    var productsTable = document.getElementById("productsTable");

    productsTable.innerHTML = tableHeader + newRow + newRowButton;
}

function insertNewRow() {
    var id = document.getElementById("productId");
    var name = document.getElementById("productName");
    var price = document.getElementById("productPrice");

    var productsTable = document.getElementById("productsTable");
    var length = productsTable.rows.length;


    if (validated(id, name, price)) {
        var row = productsTable.insertRow(length++);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);

        cell1.innerHTML = id.value;
        cell2.innerHTML = name.value;
        cell3.innerHTML = price.value;

        id.value = "";
        name.value = "";
        price.value = "";

        document.getElementById("productId").focus();
    }

}

function validated(id, name, price) {
    var status = true;

    var idPat = /\\d{1,}/, namePat = /\\w(\\s)?((\\w)?(\\w)?)?/, pricePat = /\\d{1,}/;

    //	status = idPat.test(id.value) && namePat.test(name.value) && pricePat.test(price.value);

    return status;
}

