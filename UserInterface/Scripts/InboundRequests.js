﻿var productId, productName, requiredQuantity, availableQuantity;
var row, cell1, cell2, cell3, cell4, cell5;
var id = 0;
var requestsTable = "";

function init() {
    requestsTable = document.getElementById("requestsTable");
}

var operationsCell = '<td>\
							<button class="btn btn-success btn-sm"><span class="glyphicon glyphicon-hand-up"> Fulfill</button> \
					</td> ';

function addDummyValues() {
    init();
    for (var i = 0; i < 10; i++) {
        createNewRow((i + 1), "Product " + (i + 1), Math.ceil((1000 * Math.random()) / Math.random()), Math.floor((100 * Math.random()) * 2));
    }
}

function createNewRow(productId, productName, requiredQuantity, availableQuantity) {
    this.productId = productId;
    this.productName = productName;
    this.requiredQuantity = requiredQuantity;
    this.availableQuantity = availableQuantity;

    var length = requestsTable.rows.length;
    row = requestsTable.insertRow(length);
    cell1 = row.insertCell(0);
    cell2 = row.insertCell(1);
    cell3 = row.insertCell(2);
    cell4 = row.insertCell(3);
    cell5 = row.insertCell(4);

    cell1.innerHTML = productId;
    cell2.innerHTML = productName;
    cell3.innerHTML = requiredQuantity;
    cell4.innerHTML = '<input type="number" class="form-control" value=' + availableQuantity + ' id=' + (id) + ' onchange="validateQuantity(' + (id++) + ')">';
    cell5.innerHTML = operationsCell;
}

function validateQuantity(validationElementId) {
    var tab = document.getElementById('requestsTable').rows[validationElementId];
    requiredQuantity = tab.cells[2].value;
    availableQuantity = tab.cells[3].value;

    if (availableQuantity > requiredQuantity) {

    }
}