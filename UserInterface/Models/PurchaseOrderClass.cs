﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UserInterface.FinalPurchaseDisplayService;
using UserInterface.PurchaseOrderService;
using UserInterface.RequestDetailService;

namespace UserInterface.Models
{
    public class PurchaseOrderClass
    {
        public RequestDetail requestDetail = new RequestDetail();
        public List<UserInterface.PurchaseOrderService.RequestProductMap> purchaseProductList = new List<UserInterface.PurchaseOrderService.RequestProductMap>();

        //DropDownDisplayList
        public List<List<ProductVendorMap>> listOfVendorList = new List<List<ProductVendorMap>>();

        //DropDownSelectList
        public List<int> selectedVendorList = new List<int>();


        //SubOrderList
        public List<SubOrder> subOrderList = new List<SubOrder>();

        public PurchaseOrderClass(RequestDetail request, List<UserInterface.PurchaseOrderService.RequestProductMap> purchaseList, List<List<ProductVendorMap>> listOfVList)
        {
            requestDetail = request;
            purchaseProductList = purchaseList;
            listOfVendorList = listOfVList;
        }

        public PurchaseOrderClass(RequestDetail request, List<SubOrder> soList)
        {
             requestDetail = request;
             subOrderList = soList;
        }
    }
}