﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UserInterface.RequestDetailService;
namespace UserInterface.Models
{
    public class RequestDetailRetrievalClass
    {
        public RequestDetail requestDetail= new RequestDetail();

        public List<RequestProductMap> productList = new List<RequestProductMap>();

        public RequestDetailRetrievalClass(RequestDetail request, List<RequestProductMap> pList)
        {
            requestDetail = request;
            productList = pList;
        }
    }
}