﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UserInterface.RequestDetailService;
using UserInterface.Models;
using UserInterface.PurchaseOrderService;
using UserInterface.FinalPurchaseDisplayService;


namespace UserInterface.Controllers
{
    public class RequestController : Controller
    {
        RequestDetailsRetrievalClient retrieve = new RequestDetailsRetrievalClient();
        DisplayFinalPurchaseClient displayClient = new DisplayFinalPurchaseClient();

        public ActionResult RequestDetail(string requestId)
        {
            int intRequestId = Convert.ToInt32(requestId);
            RequestDetail requestDetails = retrieve.getRequestDetail(intRequestId);

            List<UserInterface.RequestDetailService.RequestProductMap> productList = retrieve.getRequestProductsDetails(intRequestId);

            return View(new RequestDetailRetrievalClass(requestDetails, productList));          
        }




        public ActionResult PurchaseOrder(FormCollection collection)
        {
            int intRequestId = Convert.ToInt32(collection["requestDetail.requestId"]);

            RequestDetail requestDetails = retrieve.getRequestDetail(intRequestId);


            string productID = collection["product.productId"];
            int[] productIDArray = productID.Split(',').Select(p => Convert.ToInt32(p)).ToArray();

            string allocation = collection["product.allocatedQuantity"];
            int[] allocationArray = allocation.Split(',').Select(p => Convert.ToInt32(p)).ToArray();

            string required = collection["product.requiredQuantity"];
            int[] requiredArray = required.Split(',').Select(p => Convert.ToInt32(p)).ToArray();

            string available = collection["product.availableQuantity"];
            int[] availableArray = available.Split(',').Select(p => Convert.ToInt32(p)).ToArray();


            PurchaseOrderServiceClient purchaseOrder = new PurchaseOrderServiceClient();

            List<UserInterface.PurchaseOrderService.RequestProductMap> productList = purchaseOrder.getPurchaseProductDetails(productIDArray.ToList(), allocationArray.ToList(), requiredArray.ToList(), intRequestId);
            purchaseOrder.updateProductInventory(productIDArray.ToList(), allocationArray.ToList(), availableArray.ToList());


            if (productList != null)
            {
                var purchaseProductID = productList.Select(i => i.productId).ToArray();

                List<List<ProductVendorMap>> listOfVendorList = purchaseOrder.getVendorProductList(purchaseProductID.ToList(), intRequestId);
                return View(new PurchaseOrderClass(requestDetails, productList, listOfVendorList));
            }

            else
            {
                retrieve.changeRequestStatusToGranted(intRequestId);
                return RedirectToAction("Requests", "Admin");
            }


        }




        public ActionResult DisplayPurchase(FormCollection collection)
        {
            int requestID = Convert.ToInt32(collection["requestDetail.requestId"]);
            RequestDetail requestDetails = retrieve.getRequestDetail(requestID);

            string productID = collection["record.Product.productId"];
            int[] productIDArray = productID.Split(',').Select(p => Convert.ToInt32(p)).ToArray();

            string purchase = collection["record.Product.purchaseQuantity"];
            int[] purchaseArray = purchase.Split(',').Select(p => Convert.ToInt32(p)).ToArray();

            string vendorID = collection["selectedVendorList"];
            int[] vendorIDArray = vendorID.Split(',').Select(p => Convert.ToInt32(p)).ToArray();

            List<SubOrder> subOrderList = displayClient.getSubOrders(requestID, productIDArray.ToList(), purchaseArray.ToList(), vendorIDArray.ToList());

            return View(new PurchaseOrderClass(requestDetails, subOrderList));
        }

        [HttpPost]
        public ActionResult DisplayPurchasePost(FormCollection collection)
        {
            int requestID = Convert.ToInt32(collection["requestDetail.requestId"]);
            retrieve.changeRequestStatusToSent(requestID);

            return RedirectToAction("Requests","Admin");
        }


    }
}
