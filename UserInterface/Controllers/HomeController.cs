﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UserInterface.AuthenticationService;
using UserInterface.AssetRequestService;

namespace UserInterface.Controllers
{
    public class HomeController : Controller
    {
        static List<RequestProductItem> itemRequestList = new List<RequestProductItem>();

        AuthenticateClient client = new AuthenticateClient();
        AssetManagementClient clientRequest = new AssetManagementClient();

        LoginClass user = new LoginClass();

        public ActionResult AssetRequestPage(List<RequestProductItem> item)
        {
            return View(item);
        }
       
        [HttpPost]
        public ActionResult AssetRequestPage(RequestProduct request, List<RequestProductItem> item)
        {
            clientRequest.setRequests(request, item);
            return View();
        }

        public ActionResult ViewRequests()
        {
            List<RequestProduct> requestList = clientRequest.getRequests();

            return View(requestList);
        }

        public ActionResult Index()
        {
            Session["UserID"] = null;
            Session["Authority"] = 0;
            return View();
        }

        [HttpPost]
        public ActionResult Index(LoginClass user)
        {
            int authority = client.AuthenticateUser(user);

            if (authority == 1)
            {
                Session["UserID"] = user.userName;
                Session["Authority"] = authority;
                return RedirectToAction("Admin");
            }

            else if (authority == 2)
            {
                Session["UserID"] = user.userName;
                Session["Authority"] = authority;
                return RedirectToAction("ApproverLogin");
            }

            else if (authority == 3)
            {
                Session["UserID"] = user.userName;
                Session["Authority"] = authority;
                return RedirectToAction("AssetRequestPage");
            }

            else
            {
                Session["UserID"] = null;
                return View();
            }

        }

        public ActionResult Admin()
        {
            return View();
        }

        public ActionResult PurchaseHistory()
        {
            return View();
        }
       
    }
}
