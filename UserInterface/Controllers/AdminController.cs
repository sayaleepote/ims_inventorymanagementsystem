﻿using System;
using DotNet.Highcharts;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UserInterface.RequestHistoryService;
using UserInterface.PurchaseHistoryService;
using DotNet.Highcharts.Options;
using DotNet.Highcharts.Enums;
using DotNet.Highcharts.Helpers;
using UserInterface.VendorServiceReference2;

namespace UserInterface.Controllers
{
    public class AdminController : Controller
    {
        GetPurchaseHistoryClient purchaseHistoryClient = new GetPurchaseHistoryClient();
        RequestHistoryClient requestHistoryClient = new RequestHistoryClient();
        VendorServiceClient vendorServiceClient = new VendorServiceClient();

        public ActionResult Requests()
        {
            List<RequestDetail> requestList = requestHistoryClient.getRequestHistory();

            requestList = (requestList.Where(status => status.requestStatusId == 0)).ToList();


            //To order the requests Priority wise based on the Date and Time.
            requestList = requestList.OrderBy(record => record.requestDate.Year)
                            .ThenBy(record => record.requestDate.Month)
                            .ThenBy(record => record.requestDate.DayOfWeek)
                            .ThenBy(record => record.requestDate.TimeOfDay)
                            .ThenBy(record => record.requestPriority)
                            .ToList();

            return View(requestList);
        }

        public ActionResult Reports()
        {
            return View();
        }

        public ActionResult PurchaseReports()
        {
            List<PurchaseHistory> recordList = purchaseHistoryClient.getPurchaseHistory();

            var xDataMonths = recordList.Select(i => i.ArrivalDate.ToString()).ToArray();
            var yDataAmount = recordList.Select(i => new object[] { i.BillAmount }).ToArray();

            var purchaseChart = new Highcharts("chart")

                .InitChart(new Chart { DefaultSeriesType = ChartTypes.Line })

                .SetTitle(new Title { Text = "Purchase History" })

                .SetSubtitle(new Subtitle { Text = "Amount spent per Month" })

                 //set the X title
                .SetXAxis(new XAxis { Title = new XAxisTitle { Text = "Months" } })

                .SetXAxis(new XAxis { Categories = xDataMonths })

                //set the Y title
                .SetYAxis(new YAxis { Title = new YAxisTitle { Text = "Amount Spent" } })
                .SetTooltip(new Tooltip
                {
                    Enabled = true,
                    Formatter = @"function() { return '<b>'+ this.series.name +'</b><br/>'+ this.x +': '+ this.y; }"
                })
                .SetPlotOptions(new PlotOptions
                {
                    Line = new PlotOptionsLine
                    {
                        DataLabels = new PlotOptionsLineDataLabels
                        {
                            Enabled = true
                        },
                        EnableMouseTracking = false
                    }
                })

                //load the Y values 
                 .SetSeries(new[]
                 {
                        new Series {Name = "Amount", Data = new Data(yDataAmount)},
                  });

            return View(purchaseChart);
        }

        public ActionResult RequestReports()
        {
            List<RequestDetail> requestList = requestHistoryClient.getRequestHistory();

            var requestStatusArray = requestList.Select(i => i.requestStatusId).ToArray();

            int countOfPendingRequests = (requestStatusArray.Where(status => status == 0)).Count();
            int countOfAcceptedRequests = (requestStatusArray.Where(status => status == 1)).Count();
            int countOfDeclinedRequests = (requestStatusArray.Where(status => status == 2)).Count();

            var pieChartList = new List<object[]>();
            pieChartList.Add(new object[] { "Pending Requests", countOfPendingRequests });
            pieChartList.Add(new object[] { "Accepted Requests", countOfAcceptedRequests });
            pieChartList.Add(new object[] { "Declined Requests", countOfDeclinedRequests });


            var requestChart = new Highcharts("chart")
                .SetTitle(new Title { Text = "Request Status" })
                .SetSeries(new Series
                {
                    Type = ChartTypes.Pie,
                    Name = "Request History",
                    Data = new Data(pieChartList.ToArray())
                });


            return View(requestChart);
        }

        public ActionResult PurchaseHistory()
        {
            List<PurchaseHistory> historyList = purchaseHistoryClient.getPurchaseHistory();
            return View(historyList);
        }

        public ActionResult ViewVendors()
        {
            VendorServiceClient vendorReference = new VendorServiceClient();
            List<Vendor> vendorsList = vendorReference.GetAllVendors();

            return View(vendorsList);
        }

        [HttpGet]
        public ActionResult ViewVendors(string vendorID)
        {
            VendorServiceClient vendorReference = new VendorServiceClient();

            int intVendorID = Convert.ToInt32(vendorID);
            vendorReference.DeleteVendor(intVendorID);
            List<Vendor> vendorsList = vendorReference.GetAllVendors();

            return View(vendorsList);
        }

        public ActionResult AddVendor(List<vendorItem> vendorItemList)
        {
            return View(vendorItemList);
        }
        [HttpPost]
        public ActionResult AddVendor(Vendor vendor, List<vendorItem> vendorItemList)
        {
            vendorServiceClient.CreateVendor(vendor, vendorItemList);
            return View();
        }
    }
}
