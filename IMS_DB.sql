
drop table Approval;
drop table Approver;
drop table Employee;
drop table [Login];
drop table [Order];
drop table ProductDetails;
drop table ProductVendorMap;
drop table RequestDetails;
drop table RequestProductMap;
drop table SubOrder;
drop table VendorDetails;

use master;
drop database InventoryManagementSystem
create database InventoryManagementSystem 
use InventoryManagementSystem


create table Login(
  [Login_ID] varchar(20),
  [Password] varchar(50) not null,
  [Authority] int,
  PRIMARY KEY(Login_ID)
);

insert into [Login] values ('admin1','admin12',1);
insert into [Login] values ('admin2','admin123',1);
insert into [Login] values ('x3858','aprrover123', 2);
insert into [Login] values ('x3859','aprrover123', 2);
insert into [Login] values ('asset','asset123', 3);

--select * from Login

create table [VendorDetails](
	[Vendor_ID] int primary key,
	[Vendor_Name] varchar(50) not null,
	[Vendor_Contact] varchar(13) not null,
	[Vendor_Email] varchar(50) not null,
);


--select * from VendorDetails

create table [Order](
	[Order_ID] int primary key,
	[Request_ID] int not null,
	[Purchase_Date] date null,
	[Total_Amount] int not null,
	[Approval_Status] int not null
);

insert into [Order] values (1,1001,'1/11/2011',2000,1);
insert into [Order] values (2,1002,'8/17/2012',4000,1);
insert into [Order] values (3,1003,'10/20/2012',5000,0);
insert into [Order] values (4,1004,'11/21/2012',200,1);
insert into [Order] values (5,1005,'12/2/2012',2000,1);
insert into [Order] values (6,1006,'12/17/2012',4000,1);
insert into [Order] values (7,1007,'2/1/2013',3000,0);
insert into [Order] values (8,1008,'11/21/2013',200,1);

--select * from [Order]

create table [SubOrder](
	[SubOrder_ID] int primary key,
	[Order_ID] int not null,
	[Vendor_ID] int not null,
	[Product_ID] int not null,
	[Request_ID] int not null,
	[Quantity_To_Order] int not null,
	[Price] int not null,	
	[Status] int not null,
	[Invoice] varchar(60),
	[ArrivalDate] date
);

--insert into [SubOrder] ([SubOrder_ID],[Order_ID],[Vendor_ID],[Product_ID],[Quantity_To_Order],[Price],[Status],[ArrivalDate]) values (11,1,101,1234,2,14000,1,'12/01/2011');
--insert into [SubOrder] ([SubOrder_ID],[Order_ID],[Vendor_ID],[Product_ID],[Quantity_To_Order],[Price],[Status],[ArrivalDate]) values (12,1,102,1235,5,10000,1,'02/01/2011');
--insert into [SubOrder] ([SubOrder_ID],[Order_ID],[Vendor_ID],[Product_ID],[Quantity_To_Order],[Price],[Status],[ArrivalDate]) values (13,1,103,1236,2,20000,1,'01/18/2012');
--insert into [SubOrder] ([SubOrder_ID],[Order_ID],[Vendor_ID],[Product_ID],[Quantity_To_Order],[Price],[Status],[ArrivalDate]) values (21,2,101,1237,4,5600,1,'01/31/2012');
--insert into [SubOrder] ([SubOrder_ID],[Order_ID],[Vendor_ID],[Product_ID],[Quantity_To_Order],[Price],[Status],[ArrivalDate]) values (22,2,104,1238,3,25000,1,'02/27/2013');
--insert into [SubOrder] ([SubOrder_ID],[Order_ID],[Vendor_ID],[Product_ID],[Quantity_To_Order],[Price],[Status],[ArrivalDate]) values (31,3,105,1239,4,12000,1,'11/04/2013');
--insert into [SubOrder] ([SubOrder_ID],[Order_ID],[Vendor_ID],[Product_ID],[Quantity_To_Order],[Price],[Status],[ArrivalDate]) values (32,3,101,1240,2,10000,1,'08/01/2013');
--insert into [SubOrder] ([SubOrder_ID],[Order_ID],[Vendor_ID],[Product_ID],[Quantity_To_Order],[Price],[Status],[ArrivalDate]) values (33,3,102,1241,1,5000,1,'06/22/2014');

--select * from [SubOrder]

--select [Price],[ArrivalDate] from [SubOrder]

--DELETE FROM [SubOrder]


create table [ProductDetails](
	[Product_ID] int primary key,
	[Product_Name] varchar(20) not null,
	[Available_Quantity] int not null,
);

insert into [ProductDetails] values (1234,'Printer',2);
insert into [ProductDetails] values (1235,'Projector',1);
insert into [ProductDetails] values (1236,'Marker',3);
insert into [ProductDetails] values (1237,'WhiteBoard',4);
insert into [ProductDetails] values (1238,'KeyBoard',2);
insert into [ProductDetails] values (1239,'Mouse',1);
insert into [ProductDetails] values (1240,'Monitor',4);
insert into [ProductDetails] values (1241,'Telephone',2);
insert into [ProductDetails] values (1242,'FireExtinguisher',2);

--select * from [ProductDetails];

create table [ProductVendorMap](
	[Vendor_ID] int not null,
	[Vendor_Name] varchar(50) not null,
	[Product_ID] int not null,
	[Price] int not null
);


insert into [VendorDetails] values (101,'Rahul Singh','09876543210','rahulsingh@gmail.com');
insert into [VendorDetails] values (102,'Kunal Khanna','09877743210','kunalkhanna@yahoo.com');
insert into [VendorDetails] values (103,'Preetam Lal', '08976542341','preetamlal@gmail.com');
insert into [VendorDetails] values (104,'Diksha Talwar', '07765482964','dikshatalwar@gmail.com');
insert into [VendorDetails] values (105,'Preeti Mishra', '09893737322','preetimishra@gmail.com');

insert into [ProductVendorMap] values(101,'Rahul Singh',1234,7000);
insert into [ProductVendorMap] values(102,'Kunal Khanna',1234,8000);
insert into [ProductVendorMap] values(103,'Preetam Lal',1234,9000);

insert into [ProductVendorMap] values(101,'Rahul Singh',1235,5200);
insert into [ProductVendorMap] values(104,'Diksha Talwar',1235,5400);
insert into [ProductVendorMap] values(105,'Preeti Mishra',1235,4200);

insert into [ProductVendorMap] values(102,'Kunal Khanna',1236,20);
insert into [ProductVendorMap] values(103,'Preetam Lal',1236,15);

insert into [ProductVendorMap] values(105,'Preeti Mishra',1237,1200);
insert into [ProductVendorMap] values(102,'Kunal Khanna',1237,1320);
insert into [ProductVendorMap] values(103,'Preetam Lal',1237,1115);

insert into [ProductVendorMap] values(101,'Rahul Singh',1238,720);
insert into [ProductVendorMap] values(103,'Preetam Lal',1238,515);

insert into [ProductVendorMap] values(101,'Rahul Singh',1239,500);
insert into [ProductVendorMap] values(103,'Preetam Lal',1239,100);
insert into [ProductVendorMap] values(104,'Diksha Talwar',1239,250);
insert into [ProductVendorMap] values(105,'Preeti Mishra',1239,450);

insert into [ProductVendorMap] values(104,'Diksha Talwar',1240,3250);
insert into [ProductVendorMap] values(105,'Preeti Mishra',1240,5450);

insert into [ProductVendorMap] values(101,'Rahul Singh',1241,3250);
insert into [ProductVendorMap] values(102,'Kunal Khanna',1241,3450);

--select * from [ProductVendorMap];


CREATE TABLE [RequestDetails]
(
	[requestId] INT NOT NULL PRIMARY KEY, 
    [requesterId] INT NOT NULL, 
    [requestPriority] INT NOT NULL, 
    [requestDate] DATE NOT NULL, 
    [requestStatusId] INT NOT NULL
);

insert into [RequestDetails] values (10001,3858,1,'11/03/2012',1);
insert into [RequestDetails] values (10002,3859,3,'12/07/2012',0);
insert into [RequestDetails] values (10003,3860,1,'02/23/2013',1);
insert into [RequestDetails] values (10004,3861,2,'12/03/2013',0);
insert into [RequestDetails] values (10005,3862,3,'01/13/2014',1);
insert into [RequestDetails] values (10006,3863,1,'03/24/2014',0);
insert into [RequestDetails] values (10007,3864,2,'03/28/2015',2);
insert into [RequestDetails] values (10008,3865,3,'12/03/2015',0);
insert into [RequestDetails] values (10009,3866,1,'12/03/2015',0);
insert into [RequestDetails] values (10010,3867,2,'12/04/2015',0);
insert into [RequestDetails] values (10011,3868,3,'12/04/2015',0);
insert into [RequestDetails] values (10012,3869,1,'12/04/2015',0);

--select * from RequestDetails
--delete from RequestDetails

CREATE TABLE [RequestProductMap]
(
    [RequestId] INT NOT NULL, 
    [ProductId] INT NOT NULL, 
    [ProductName] VARCHAR(50) NOT NULL, 
    [RequiredQnt] INT NOT NULL, 
    [AllocatedQnt] INT,
	[PurchaseQnt] INT
)

--select * from [RequestProductMap]

insert into [RequestProductMap] ([RequestId],[ProductId],[ProductName],[RequiredQnt]) values (10012,1241,'Telephone',4);
insert into [RequestProductMap] ([RequestId],[ProductId],[ProductName],[RequiredQnt]) values (10012,1240,'Monitor',4);
insert into [RequestProductMap] ([RequestId],[ProductId],[ProductName],[RequiredQnt]) values (10012,1235,'Projector',2);


CREATE TABLE [dbo].[Approval] (
    [Request_ID]       INT NOT NULL,
    [TechLead]        INT NULL,
    [TechLead_Status] INT NULL,
    [ProjectManager]        INT NULL,
    [ProjectManager_Status] INT NULL,
    [HRManager]        INT NULL,
    [HRManager_Status] INT NULL,
    [SrHRManager]        INT NULL,
    [SrHRManager_Status] INT NULL,
    [BranchManager]        INT NULL,
    [BranchManager_Status] INT NULL,
    [CFO] INT NULL,
    [CFO_Status] INT NULL,
    CONSTRAINT [PK_Approval] PRIMARY KEY CLUSTERED ([Request_ID] ASC)
);

CREATE TABLE [dbo].[Approver]
(
	[Emp_ID] INT NOT NULL PRIMARY KEY, 
    [Approver_ID] INT NULL, 
    [Approver_Name] VARCHAR(50) NULL, 
    [Approver_Designation] VARCHAR(50) NULL, 
    [Approval_Limit] INT NULL
)