﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Runtime.InteropServices;
using Outlook = Microsoft.Office.Interop.Outlook;
using Office = Microsoft.Office.Core;

namespace Email
{
    class Program
    {

        //method to send email to outlook
        public static void sendEMailThroughOUTLOOK()
        {
            try
            {
                // Create the Outlook application.
                Outlook.Application oApp = new Outlook.Application();

                // Create a new mail item.
                Outlook.MailItem oMsg = (Outlook.MailItem)oApp.CreateItem(Outlook.OlItemType.olMailItem);

                // Set HTMLBody. 
                //add the body of the email
                oMsg.HTMLBody = "Hello,(Body here)!";

                //Add an attachment.
                String sDisplayName = "AttachmentName";
                int iPosition = (int)oMsg.Body.Length + 1;
                int iAttachType = (int)Outlook.OlAttachmentType.olByValue;

                //now attached the file
                Outlook.Attachment oAttach = oMsg.Attachments.Add(@"D:\\Hello.txt", iAttachType, iPosition, sDisplayName);

                //Subject line
                oMsg.Subject = "Test Message!";

                // Add a recipient.
                Outlook.Recipients oRecips = (Outlook.Recipients)oMsg.Recipients;

                // Change the recipient in the next line if necessary.
                Outlook.Recipient oRecip = (Outlook.Recipient)oRecips.Add("sayaleepote44@gmail.com");

                oRecip.Resolve();

                // Send.
                oMsg.Send();

                // Clean up.
                oRecip = null;
                oRecips = null;
                oMsg = null;
                oApp = null;

                Console.WriteLine("Mail Sent");
            }//end of try block

            catch (Exception ex)
            {
                Console.WriteLine("Mail Not sent");
            }//end of catch
        
        }//end of Email Method

        static void Main(string[] args)
        {
            sendEMailThroughOUTLOOK();

        }
        
    }
}
