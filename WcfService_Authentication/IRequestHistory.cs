﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfService_Authentication
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IRequestHistory" in both code and config file together.
    [ServiceContract]
    public interface IRequestHistory
    {
        [OperationContract]
        List<RequestDetail> getRequestHistory();      
    }

    [DataContract]
    public class RequestDetail
    {
      
        [DataMember]
        public int requestId { get; set; }

        [DataMember]
        public int requesterId { get; set; }

        [DataMember]
        public int requestPriority { get; set; }

        [DataMember]
        public DateTime requestDate { get; set; }

        [DataMember]
        public int requestStatusId { get; set; }


    }

    [DataContract]
    public class RequestProductMap
    {
        [DataMember]
        public int requestId { get; set; }

        [DataMember]
        public int productId { get; set; }

        [DataMember]
        public string productName { get; set; }

        [DataMember]
        public int requiredQuantity { get; set; }

        [DataMember]
        public int availableQuantity { get; set; }
        
        [DataMember]
        public int allocatedQuantity { get; set; }

        [DataMember]
        public int purchaseQuantity { get; set; }

        

    }
}
