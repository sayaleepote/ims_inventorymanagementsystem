﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfService_Authentication
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IVendorService" in both code and config file together.
    [ServiceContract]
    public interface IVendorService
    {
        [OperationContract]
        List<Vendor> GetAllVendors();

        [OperationContract]
        Vendor GetVendorDetails(int vendorID);

        [OperationContract]
        int DeleteVendor(int vendorID);

        [OperationContract]
        int UpdateVendor(Vendor vendor);

        [OperationContract]
        int CreateVendor(Vendor vendor, List<vendorItem> vendorItemList);
    }

    [DataContract]
    public class vendorItem
    {
        private int productIdRef;
        private string productNameRef;
        private int productPriceRef;

        public vendorItem()
        {

        }
        public vendorItem(int _pID, string _pName, int _pPrice)
        {
            // TODO: Complete member initialization
            this.productIdRef = _pID;
            this.productNameRef = _pName;
            this.productPriceRef = _pPrice;
        }

        [DataMember]
        public int productId { get; set; }

        [DataMember]
        public string productName { get; set; }

        [DataMember]
        public int productPrice { get; set; }

    }
}
