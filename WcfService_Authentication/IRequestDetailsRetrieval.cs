﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfService_Authentication
{
   
    [ServiceContract]
    public interface IRequestDetailsRetrieval
    {
        [OperationContract]
        RequestDetail getRequestDetail(int requestId);

        [OperationContract]
        List<RequestProductMap> getRequestProductsDetails(int requestId);

        [OperationContract]
        void changeRequestStatusToSent(int requestId);

        [OperationContract]
        void changeRequestStatusToGranted(int requestId);
    }
}
