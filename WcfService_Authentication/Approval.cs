﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.ServiceModel;
using System.Runtime.Serialization;
using WcfService_Authentication;

namespace WcfService_Authentication
{
    [DataContract]
    public enum APPROVAL_STATUS
    {
        PENDING = 0,
        APPROVED,
        DENIED
    }

    [DataContract]
    public class Approval : IApprovalService
    {
        private int _RequestID;

        private SqlConnection _connection = new SqlConnection(DBConnection.connectionString);

        [DataMember]
        public int RequestID
        {
            get { return _RequestID; }
            set { _RequestID = value; }
        }

        public int CreateApprovalRequest(int _OrderID, int _RequestID)
        {
            int status = 1;
            int requestId, requesterId, requestPriority, requestStatusId, approvalLimit, totalAmount;
            requestId = requesterId = requestPriority = requestStatusId = approvalLimit = totalAmount = 0;
            int approverId = 0;
            DateTime requestDate;


            //1) get required data
            string _statement = "select * from RequestDetails where requestId = " + _RequestID + "";

            SqlCommand _command = new SqlCommand(_statement, _connection);
            
            _connection.Open();
            SqlDataReader data = _command.ExecuteReader();

            if (data.Read())
            {
                requestId = data.GetInt32(0);
                requesterId = data.GetInt32(1);
                requestPriority = data.GetInt32(2);
                requestDate = data.GetDateTime(3);
                requestStatusId = data.GetInt32(4);
            }
            else
            {
                status = -1;
            }
            _connection.Close();
            //

            //2) get requester details
            _statement = "select Approver_Limit from Approver where Emp_ID = " + requesterId + "";
            _command = new SqlCommand(_statement, _connection);

            _connection.Open();
            data = _command.ExecuteReader();
            if (data.Read())
            {
                approvalLimit = data.GetInt32(0);
            }
            else
            {
                status = -1;
            }
            _connection.Close();
            //

            //3) get total amount required to be approved
            _statement = "select Total_Amount from Order where Order_ID = " + _OrderID + "";
            _command = new SqlCommand(_statement, _connection);

            _connection.Open();
            data = _command.ExecuteReader();

            if (data.Read())
            {
                totalAmount = data.GetInt32(0);
            }
            else
            {
                status = -1;
            }
            _connection.Close();
            //
            int approverNumber = 0;

            while (approvalLimit < totalAmount)
            {
                approverNumber++;

                //4) get approver id of current emloyee
                _statement = "select Approver_ID from Approver where Emp_ID = " + requesterId + "";
                _command = new SqlCommand(_statement, _connection);

                _connection.Open();
                data = _command.ExecuteReader();

                if (data.Read())
                {
                    approverId = data.GetInt32(0);
                }
                else
                {
                    status = -1;
                }
                _connection.Close();
                //

                //5) add new approver
                AddApprover(_RequestID, approverId, approverNumber);
                //

                //set current approver as requester to check his approval limit
                requesterId = approverId;
                _statement = "select Approver_Limit from Approver where Emp_ID = " + requesterId + "";
                _command = new SqlCommand(_statement, _connection);

                _connection.Open();
                data = _command.ExecuteReader();
                if (data.Read())
                {
                    approvalLimit = data.GetInt32(0);
                }
                else
                {
                    status = -1;
                }
                _connection.Close();
                //
                status = 0;
            }

            return status;
        }

        public int AddApprover(int _RequestID, int _ApproverID, int _ApproverNumber)
        {
            //add new approver in Approval table
            //string _statement = "insert into Approval values ("+_RequestID+", "++")";
            int status = 1;
            int _ApproverDefaultStatus = 0;

            //get approver's designation
            string _Approver = "";

            string _statement = "select Approver_Designation from Approver where Approver_ID = "+_ApproverID+"";
            SqlCommand _command = new SqlCommand(_statement, _connection);

            _connection.Open();
            SqlDataReader data = _command.ExecuteReader();

            if (data.Read())
            {
                _Approver = data.GetString(0);
            }
            _connection.Close();
            //

            //see if request_ID already exists
            _statement = "select count(Request_ID) from Approval where Request_ID = "+_RequestID+"";
            _command = new SqlCommand(_statement, _connection);

            _connection.Open();

            data = _command.ExecuteReader();
            int count = 0;

            if (data.Read())
            {
                count = data.GetInt32(0);
            }
            else
            {
                status = -1;
            }
            _connection.Close();
            //

            if (count > 0) //request exists then
            {
                //update existing request

                //update according to new database definition
                //string _statement = "update Approval set Approver" + _ApproverNumber + " = " + _ApproverID + ", Approver"+_ApproverNumber+"_Status = 0 where Request_ID = " + _RequestID + "";
                _statement = "update Approval set " + _Approver + " = "+_ApproverID+", "+_Approver+"_Status = "+_ApproverDefaultStatus+" where Request_ID = "+_RequestID+"";
                _command = new SqlCommand(_statement, _connection);

                _connection.Open();
                status = (_command.ExecuteNonQuery() > 0) ? 0 : -1;
                _connection.Close();
            }
            else //request doesn't exist then
            {
                //create new request

                //update according to new database definition
                //_statement = "insert into Approval (Request_ID, Approver1, Approver1_Status) values ("+_RequestID+","+_ApproverID+",0)";
                _statement = "insert into Approval (Request_ID, " + _Approver + ", " + _Approver + "_Status) values (" + _RequestID + ", " + _ApproverID + ", " + _ApproverDefaultStatus + ")";

                _command = new SqlCommand(_statement, _connection);

                _connection.Open();
                status = (_command.ExecuteNonQuery() > 0) ? 0 : -1;
                _connection.Close();
            }

            return status;
        }
    }
}