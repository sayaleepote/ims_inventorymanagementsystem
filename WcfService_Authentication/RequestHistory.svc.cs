﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfService_Authentication
{
    public class RequestHistory : IRequestHistory
    {
        string connectionString = ConfigurationManager.ConnectionStrings
                                   ["DBconnection"].ConnectionString;

        public List<RequestDetail> getRequestHistory()
        {
            List<RequestDetail> requestList = new List<RequestDetail>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {

                string query = "SELECT * FROM [RequestDetails]";

                connection.Open();
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    using (SqlDataReader sqlReader = command.ExecuteReader())
                    {

                        while (sqlReader.Read())
                        {
                            RequestDetail request = new RequestDetail();

                            request.requestId = (int)sqlReader.GetValue(sqlReader.GetOrdinal("requestId"));                            
                            request.requesterId = (int)sqlReader.GetValue(sqlReader.GetOrdinal("requesterId"));
                            request.requestPriority = (int)sqlReader.GetValue(sqlReader.GetOrdinal("requestPriority"));
                            request.requestDate = Convert.ToDateTime(sqlReader.GetValue(sqlReader.GetOrdinal("requestDate")));
                            request.requestStatusId = (int)sqlReader.GetValue(sqlReader.GetOrdinal("requestStatusId"));
                            requestList.Add(request);

                        }
                    }
                }
            }

            return requestList;
        }
      
    }
}
