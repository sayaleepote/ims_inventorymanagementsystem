﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfService_Authentication
{

    [ServiceContract]
    public interface IPurchaseOrderService
    {
        [OperationContract]
        List<RequestProductMap> getPurchaseProductDetails(int[] productIDs, int[] allocatedQnts, int[] requiredQnts, int requestID);

        [OperationContract]
        void updateProductInventory(int[] productIDs, int[] allocationQnts, int[] availableQnts);

        [OperationContract]
        List<List<ProductVendorMap>> getVendorProductList(int[] productIDs, int requestID);
    }

    [DataContract]
    public class ProductVendorMap
    {
        [DataMember]
        public int vendorID { get; set; }

        [DataMember]
        public string vendorName { get; set; }

        [DataMember]
        public int productID { get; set; }

        [DataMember]
        public int price { get; set; }
    }

}
