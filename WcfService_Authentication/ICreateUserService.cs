﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfService_Authentication
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICreateUserService" in both code and config file together.
    [ServiceContract]
    public interface ICreateUserService
    {
        [OperationContract]
        int AddNewUser(string _HashedUserName, string _HashedPassword, int _Role);
    }
}
