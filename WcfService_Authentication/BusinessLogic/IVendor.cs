﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WcfService_Authentication.BusinessLogic;

namespace WcfService_Authentication
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IVendor" in both code and config file together.
    [ServiceContract]
    public interface IVendor
    {
        [OperationContract]
        List<Vendor> GetAllVendors();

        [OperationContract]
        Vendor GetVendorDetails(int vendorID);

        [OperationContract]
        int DeleteVendor(int vendorID);

        [OperationContract]
        int UpdateVendor(Vendor vendor);

        [OperationContract]
        int CreateVendor(Vendor vendor);
    }

    //[DataContract]
    //public class Vendor
    //{
    //    [DataMember]
    //    public int _VendorID;

    //    [DataMember]
    //    public string _VendorName;

    //    [DataMember]
    //    public string _VendorContact;

    //    [DataMember]
    //    public string _VendorEmail;

    //    public Vendor()
    //    {

    //    }

    //    public Vendor(int ID, string name, string contact, string email)
    //    {
    //        _VendorID = ID;
    //        _VendorName = name;
    //        _VendorContact = contact;
    //        _VendorEmail = email;
    //    }
    //}
}
