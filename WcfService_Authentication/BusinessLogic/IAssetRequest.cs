﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Runtime.Serialization;
using System.Data;

namespace WcfService_Authentication
{
    [ServiceContract]
    interface IAssetManagement
    {

        [OperationContract]
        DataSet getRequests(RequestProduct user);

        [OperationContract]
        void setRequests(RequestProduct user);

        [DataMember]
        int requestStatus { get; set; }
    }

    [DataContract]
    public class RequestProduct
    {
        [DataMember]
        public int requestId { get; set; }

        [DataMember]
        public string requesterId { get; set; }

        [DataMember]
        public int requestPriority { get; set; }

        [DataMember]
        public DateTime requesterDate { get; set; }

        [DataMember]
        public DateTime requesterStatusId { get; set; }


    }
}