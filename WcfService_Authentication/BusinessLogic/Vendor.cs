﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.ServiceModel;
using System.Runtime.Serialization;
using WcfService_Authentication;

namespace WcfService_Authentication.BusinessLogic
{
    public class Vendor : IVendorService
    {
        [DataMember]
        public int _VendorID;

        [DataMember]
        public string _VendorName;

        [DataMember]
        public string _VendorContact;

        [DataMember]
        public string _VendorEmail;

        SqlConnection _connection = new SqlConnection(DBConnection.connectionString);

        public Vendor()
        {

        }

        public Vendor(int ID, string name, string contact, string email)
        {
            _VendorID = ID;
            _VendorName = name;
            _VendorContact = contact;
            _VendorEmail = email;
        }

        public List<Vendor> GetAllVendors()
        {
            List<Vendor> _VendorsList = new List<Vendor>();

            string _SelectVendorsQuery = "select * from VendorDetails";

            SqlCommand _command = new SqlCommand(_SelectVendorsQuery, _connection);

            _connection.Open();
            SqlDataReader _DataReader = _command.ExecuteReader();

            while (_DataReader.Read())
            {
                Vendor vendor = new Vendor(_DataReader.GetInt32(1), _DataReader.GetString(2), _DataReader.GetString(3), _DataReader.GetString(4));
                _VendorsList.Add(vendor);
            }

            _connection.Close();

            return _VendorsList;
        }

        public Vendor GetVendorDetails(int vendorID)
        {
            throw new NotImplementedException();
        }

        public int DeleteVendor(int vendorID)
        {
            throw new NotImplementedException();
        }

        public int UpdateVendor(Vendor vendor)
        {
            throw new NotImplementedException();
        }

        public int CreateVendor(Vendor vendor)
        {
            int status = 0;

            string query = @"insert into VendorDetails values()";
            //SqlCommand = new SqlCommand(query, connection);

            return status;
        }
    }
}