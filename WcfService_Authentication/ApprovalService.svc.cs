﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfService_Authentication
{
    public class ApprovalService : IApprovalService
    {
        private Approval approvalRef = new Approval();

        public int CreateApprovalRequest(int _OrderID, int _RequestID)
        {
            return approvalRef.CreateApprovalRequest(_OrderID, _RequestID);
        }

        public int AddApprover(int _RequestID, int _ApproverID, int _ApproverNumber)
        {
            return approvalRef.AddApprover(_RequestID, _ApproverID, _ApproverNumber);
        }
    }
}
