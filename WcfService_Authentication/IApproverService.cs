﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfService_Authentication
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IApproverService" in both code and config file together.
    [ServiceContract]
    public interface IApproverService
    {
        [OperationContract]
        int ApproveRequest(int _RequestID, int _ApproverID, int _OrderID);

        [OperationContract]
        int DenyRequest(int _RequestID, int _ApproverID, int _OrderID);

        [OperationContract]
        int DelayRequest(int _RequestID, int _ApproverID, int _OrderID);
    }
}
