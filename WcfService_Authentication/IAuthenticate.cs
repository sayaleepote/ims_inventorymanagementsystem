﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfService_Authentication
{
    [ServiceContract]
    public interface IAuthenticate
    {
       [OperationContract]
       int AuthenticateUser(LoginClass user);
    }

    [DataContract]
    public class LoginClass
    {
        [DataMember]
        public string userName { get; set; }

        [DataMember]
        public string password { get; set; }

        [DataMember]
        public int authority { get; set; }
       
    }
}
