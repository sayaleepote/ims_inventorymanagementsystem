﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using System.Runtime.Serialization;
using WcfService_Authentication;
using System.Data;
using System.Data.SqlClient;

namespace WcfService_Authentication
{
    [DataContract]
    public enum APPROVAL
    {
        PENDING = -1,
        APPROVED = 0,
        DENIED = 1
    }

    [DataContract]
    public class Approver : IApproverService
    {
        private SqlConnection _connection = new SqlConnection(DBConnection.connectionString);
        private int _EmployeeID;
        private int techleadApproval, projectManagerApproval, HRManagerApproval, srHRManagerApproval, branchManagerApproval, CFOApproval = (int)APPROVAL.PENDING;
        private string _statement = "";
        private SqlCommand _command = null;
        private SqlDataReader data = null;

        [DataMember]
        public int EmployeeID
        {
            get { return _EmployeeID; }
        }

        private int _ApproverID;

        [DataMember]
        public int ApproverID
        {
            get { return _ApproverID; }
        }

        private string _ApproverName;

        [DataMember]
        public string ApproverName
        {
            get { return _ApproverName; }
        }

        private string _ApproverDepartment;

        [DataMember]
        public string ApproverDepartment
        {
            get { return _ApproverDepartment; }
        }

        private int _ApprovalLimit;

        [DataMember]
        public int ApprovalLimit
        {
            get { return _ApprovalLimit; }
        }

        private string getApprover(int _ApproverID)
        {
            string Approver = "";
            //algorithm step 2
            _statement = "select Approver_Designation from Approver where Emp_ID = " + _ApproverID + "";
            _command = new SqlCommand(_statement, _connection);

            _connection.Open();
            data = _command.ExecuteReader();
            if (data.Read())
            {
                Approver = data.GetString(0);
            }
            _connection.Close();
            //
            return Approver;
        }

        private int getApproversStatus()
        {
            int status = 1;
            _statement = "select TechLead_Status, ProjectManager_Status, HRManager_Status, SrHRManager_Status, BranchManager_Status, CFO_Status from Approval";
            _command = new SqlCommand(_statement, _connection);

            _connection.Open();
            data = _command.ExecuteReader();

            if (data.Read())
            {
                techleadApproval = data.GetInt32(0);
                projectManagerApproval = data.GetInt32(1);
                HRManagerApproval = data.GetInt32(2);
                srHRManagerApproval = data.GetInt32(3);
                branchManagerApproval = data.GetInt32(4);
                CFOApproval = data.GetInt32(5);
            }
            else
                status = -1;

            _connection.Close();

            return status;
        }

        private int setApprovalStatus(int _RequestID, string _Approver, int _ApprovalStatus)
        {
            int status = 1;

            _statement = "update Approval set " + _Approver + "_Status = " + _ApprovalStatus + " where Request_ID = " + _RequestID + "";
            _command = new SqlCommand(_statement, _connection);
            _connection.Open();
            status = (_command.ExecuteNonQuery() > 0) ? 0 : 1;
            _connection.Close();
            return status;
        }

        private int setFinalApprovalStatus(int _FinalStatus, int _OrderID)
        {
            int status = 1;

            _statement = "update Order set Approval_Status = " + _FinalStatus + " where Order_ID = " + _OrderID + "";
            _command = new SqlCommand(_statement, _connection);

            _connection.Open();
            status = (_command.ExecuteNonQuery() > 0) ? 0 : 1;
            _connection.Close();
            
            return status;
        }

        //1) Algorithm ApproveRequest(requestID, approverID, orderID
        //2) Approver <= select Approver_Designation from Approver where Emp_ID = approverID
        //3) Update Approval set "+Approver+"_Status = 1 where Request_ID = requestID;
            //3.1) Set status as 0; 0:Successful, 1:Error_While_Approval_In_Progress, -1:Could Not Process Approval Due to Other Reasons
        //4) update Approval_Status in Order table (overall approval of Order)
            //4.1) get status of all participants from Approval table
            //4.2) considering 999 for Not Applicable, if each approval_status > 0 then Approval_Status = APPROVED else PENDING
        public int ApproveRequest(int _RequestID, int _ApproverID, int _OrderID)
        {
            int status = -1;

            //algorithm step 2
            string _Approver = getApprover(_ApproverID);

            //algorithm step 3
            status = setApprovalStatus(_RequestID, _Approver, (int)APPROVAL.APPROVED);

            //algorithm step 4
            status = getApproversStatus();

            //if everyone's approval status > 0 then consider it APPROVED else PENDING
            int finalApproval = (techleadApproval>0 && projectManagerApproval>0 && HRManagerApproval>0 && srHRManagerApproval>0 && branchManagerApproval>0 && CFOApproval>0) ? (int)APPROVAL.APPROVED : (int)APPROVAL.PENDING;

            status = setFinalApprovalStatus(finalApproval, _OrderID);

            return status;
        }

        public int DenyRequest(int _RequestID, int _ApproverID, int _OrderID)
        {
            int status = -1;

            //algorithm step 2
            string _Approver = getApprover(_ApproverID);

            //algorithm step 3
            status = setApprovalStatus(_RequestID, _Approver, (int)APPROVAL.DENIED);

            //algorithm step 4
            status = getApproversStatus();

            //if everyone's approval status > 0 then consider it APPROVED else PENDING
            int finalApproval = (techleadApproval < 0 || projectManagerApproval < 0 || HRManagerApproval < 0 || srHRManagerApproval < 0 || branchManagerApproval < 0 || CFOApproval < 0) ? (int)APPROVAL.DENIED : (int)APPROVAL.PENDING;

            status = setFinalApprovalStatus(finalApproval, _OrderID);

            return status;
        }

        public int DelayRequest(int _RequestID, int _ApproverID, int _OrderID)
        {
            int status = -1;

            //algorithm step 2
            string _Approver = getApprover(_ApproverID);

            //algorithm step 3
            status = setApprovalStatus(_RequestID, _Approver, (int)APPROVAL.PENDING);

            //algorithm step 4
            status = getApproversStatus();

            //if everyone's approval status > 0 then consider it APPROVED else PENDING
            int finalApproval = (int) APPROVAL.PENDING;
                if(techleadApproval == 0 || projectManagerApproval == 0 || HRManagerApproval == 0 || srHRManagerApproval == 0 || branchManagerApproval == 0 || CFOApproval == 0)
                finalApproval = (int) APPROVAL.PENDING;

            status = setFinalApprovalStatus(finalApproval, _OrderID);

            return status;
        }
    }
}