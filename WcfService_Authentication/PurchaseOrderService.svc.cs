﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfService_Authentication
{
    public class PurchaseOrderService : IPurchaseOrderService
    {
        string connectionString = ConfigurationManager.ConnectionStrings
                                   ["DBconnection"].ConnectionString;

        public List<RequestProductMap> getPurchaseProductDetails(int[] productIDs, int[] allocatedQnts, int[] requiredQnts, int requestID)
        {

            using (SqlConnection connection = new SqlConnection(connectionString))
            {

                for (int i = 0; i < productIDs.Length; i++)
                {
                    string query = "UPDATE [RequestProductMap] SET [AllocatedQnt] = @allocatedQuantity, [PurchaseQnt] = @purchaseQuantity WHERE RequestId = @requestID AND ProductId = @productID";
                   
                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@allocatedQuantity", allocatedQnts[i]);
                        command.Parameters.AddWithValue("@purchaseQuantity", (requiredQnts[i] - allocatedQnts[i]));
                        command.Parameters.AddWithValue("@requestID", requestID);
                        command.Parameters.AddWithValue("@productID", productIDs[i]);

                        connection.Open();
                        command.ExecuteNonQuery();
                        connection.Close();
                    }
                }
                
            }

            List<RequestProductMap> productList = new List<RequestProductMap>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                for (int i = 0; i < productIDs.Length; i++)
                {
                    string query = "SELECT * FROM [RequestProductMap] WHERE PurchaseQnt != 0 AND RequestId = @requestID AND ProductId = @productID";

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {

                        command.Parameters.AddWithValue("@requestID", requestID);
                        command.Parameters.AddWithValue("@productID", productIDs[i]);

                        connection.Open();
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {

                            while (sqlReader.Read())
                            {
                                RequestProductMap product = new RequestProductMap();
                                product.productId = (int)sqlReader.GetValue(sqlReader.GetOrdinal("ProductId"));
                                product.productName = (string)sqlReader.GetValue(sqlReader.GetOrdinal("ProductName"));
                                product.purchaseQuantity = (int)sqlReader.GetValue(sqlReader.GetOrdinal("PurchaseQnt"));

                                productList.Add(product);
                            }
                        }
                    }

                    connection.Close();
                }
                
            }

            return productList;
        }

        public List<List<ProductVendorMap>> getVendorProductList(int[] productIDs, int requestID)
        {

            List<List<ProductVendorMap>> listOfVendorList = new List<List<ProductVendorMap>>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {

                for (int i = 0; i < productIDs.Length; i++)
                {
                    List<ProductVendorMap> vendorList = new List<ProductVendorMap>();
                    string query = "SELECT * FROM [ProductVendorMap] WHERE Product_ID = @productID";

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@productID", productIDs[i]);

                        connection.Open();
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                ProductVendorMap singleVendorDetail = new ProductVendorMap();
                                singleVendorDetail.vendorID = (int)sqlReader.GetValue(sqlReader.GetOrdinal("Vendor_ID"));
                                singleVendorDetail.vendorName = (string)sqlReader.GetValue(sqlReader.GetOrdinal("Vendor_Name"));
                                singleVendorDetail.price = (int)sqlReader.GetValue(sqlReader.GetOrdinal("Price"));

                                vendorList.Add(singleVendorDetail);
                            }
                        }
                    }

                    listOfVendorList.Add(vendorList);
                    connection.Close();
                }

            }

            return listOfVendorList;
        }



        public void updateProductInventory(int[] productIDs, int[] allocationQnts, int[] availableQnts)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {

                for (int i = 0; i < productIDs.Length; i++)
                {
                   
                    string query = "UPDATE [ProductDetails] SET Available_Quantity = @updatedQuantity WHERE Product_ID = @productID";

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@productID", productIDs[i]);

                        int updatedQuantity = availableQnts[i] - allocationQnts[i];
                        command.Parameters.AddWithValue("@updatedQuantity", updatedQuantity);

                        connection.Open();
                        command.ExecuteNonQuery();                       
                    }
                    connection.Close();
                }

            }

        }
    }//end class
}

