﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfService_Authentication
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ApproverService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ApproverService.svc or ApproverService.svc.cs at the Solution Explorer and start debugging.
    public class ApproverService : IApproverService
    {
        private Approver approverRef = new Approver();

        public int ApproveRequest(int _RequestID, int _ApproverID, int _OrderID)
        {
            return approverRef.ApproveRequest(_RequestID, _ApproverID, _OrderID);
        }

        public int DenyRequest(int _RequestID, int _ApproverID, int _OrderID)
        {
            return approverRef.DenyRequest(_RequestID, _ApproverID, _OrderID);
        }

        public int DelayRequest(int _RequestID, int _ApproverID, int _OrderID)
        {
            return approverRef.DelayRequest(_RequestID, _ApproverID, _OrderID);
        }
    }
}
