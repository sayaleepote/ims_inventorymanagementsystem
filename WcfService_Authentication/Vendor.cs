﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.ServiceModel;
using System.Runtime.Serialization;
using WcfService_Authentication;

namespace WcfService_Authentication
{
    public class Vendor : IVendorService
    {
        [DataMember]
        public int _VendorID;

        [DataMember]
        public string _VendorName;

        [DataMember]
        public string _VendorContact;

        [DataMember]
        public string _VendorEmail;

        private SqlConnection _connection = new SqlConnection(DBConnection.connectionString);

        public Vendor()
        {
            _VendorID = 00;
            _VendorName = "Default";
            _VendorContact = "9999999999";
            _VendorEmail = "defaultEmail";
        }

        public Vendor(int ID, string name, string contact, string email)
        {
            _VendorID = ID;
            _VendorName = name;
            _VendorContact = contact;
            _VendorEmail = email;
        }

        public List<Vendor> GetAllVendors()
        {
            List<Vendor> _VendorsList = new List<Vendor>();

            string _SelectVendorsQuery = "select * from VendorDetails";

            SqlCommand _command = new SqlCommand(_SelectVendorsQuery, _connection);

            _connection.Open();

            SqlDataReader _DataReader = _command.ExecuteReader();

            while (_DataReader.Read())
            {
                Vendor vendor = new Vendor(_DataReader.GetInt32(0), _DataReader.GetString(1), _DataReader.GetString(2), _DataReader.GetString(3));
                _VendorsList.Add(vendor);
            }

            _connection.Close();

            return _VendorsList;
        }

        public Vendor GetVendorDetails(int vendorID)
        {
            Vendor _Vendor = new Vendor();

            string _SelectVendorQuery = "select * from VendorDetails where Vendor_ID = " + vendorID + "";

            SqlCommand _command = new SqlCommand(_SelectVendorQuery, _connection);

            _connection.Open();
            SqlDataReader _DataReader = _command.ExecuteReader();

            if (_DataReader.Read())
            {
                _Vendor = new Vendor(_DataReader.GetInt32(0), _DataReader.GetString(1), _DataReader.GetString(2), _DataReader.GetString(3));
            }
            _connection.Close();

            return _Vendor;
        }

        public int DeleteVendor(int vendorID)
        {
            string _DeleteVendorQuery = "delete from VendorDetails where Vendor_ID = " + vendorID + "";

            SqlCommand _command = new SqlCommand(_DeleteVendorQuery, _connection);

            _connection.Open();

            int _RowsAffected = _command.ExecuteNonQuery();

            _connection.Close();

            return _RowsAffected;
        }

        public int UpdateVendor(Vendor vendor)
        {
            string _UpdateVendorQuery = "update VendorDetails set Vendor_ID = " + vendor._VendorID + ", Vendor_Name = '" + vendor._VendorName + "', Vendor_Contact = '" + vendor._VendorContact + "', Vendor_Email = '" + vendor._VendorEmail + "'";

            SqlCommand _command = new SqlCommand(_UpdateVendorQuery, _connection);

            _connection.Open();

            int _RowsAffected = _command.ExecuteNonQuery();

            _connection.Close();

            return _RowsAffected;
        }

        public int CreateVendor(Vendor vendor, List<vendorItem> vendorItemList)
        {
            int status = 1;

            foreach (var item in vendorItemList)
            {
                int itemId = Convert.ToInt32(item.productId);
                string itemName = item.productName;
                int itemPrice = Convert.ToInt32(item.productPrice);

                //Query for total products of a vendor
                using (SqlConnection connectionForItem = new SqlConnection(DBConnection.connectionString))
                {

                    string queryItem = "insert into ProductDetails values(" + itemId + ",'" + itemName + "'," + itemPrice + ");";

                    SqlCommand commandItem = new SqlCommand(queryItem, connectionForItem);
                    connectionForItem.Open();
                    SqlDataReader sqlReaderItem = commandItem.ExecuteReader();
                    connectionForItem.Close();


                }
                //Query for products mapped to vendor
                using (SqlConnection connectionForItemMap = new SqlConnection(DBConnection.connectionString))
                {

                    string queryItemMap = "insert into ProductVendorMap values(" + vendor._VendorID + ",'" + itemId + "'," + itemPrice + ");";

                    SqlCommand commandItemMap = new SqlCommand(queryItemMap, connectionForItemMap);
                    connectionForItemMap.Open();
                    SqlDataReader sqlReaderItemMap = commandItemMap.ExecuteReader();
                    connectionForItemMap.Close();
                }

            }
            string _CreateVendorQuery = "insert into VendorDetails values(" + vendor._VendorID + ", '" + vendor._VendorName + "', '" + vendor._VendorContact + "', '" + vendor._VendorEmail + "')";

            SqlCommand _command = new SqlCommand(_CreateVendorQuery, _connection);

            _connection.Open();

            int _RowsAffected = _command.ExecuteNonQuery();

            status = _RowsAffected > 0 ? 0 : -1;

            _connection.Close();

            return status;
        }

    }
}