﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfService_Authentication
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IApprovalService" in both code and config file together.
    [ServiceContract]
    public interface IApprovalService
    {
        [OperationContract]
        int CreateApprovalRequest(int _OrderID, int _RequestID);

        [OperationContract]
        int AddApprover(int _RequestID, int _ApproverID, int _ApproverNumber);
    }
}
