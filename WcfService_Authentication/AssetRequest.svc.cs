﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace WcfService_Authentication
{
    public class AssetRequest : IAssetManagement
    {
        string connectionString = ConfigurationManager.ConnectionStrings
                                  ["DBconnection"].ConnectionString;

        public void setRequests(RequestProduct requests, List<RequestProductItem> itemList)
        {

            SqlConnection connection = new SqlConnection(connectionString);
            int requestId = Convert.ToInt32(requests.requestId);
            int requesterId = Convert.ToInt32(requests.requesterId);
            int requestPriority = Convert.ToInt32(requests.requestPriority);
            DateTime requestDate = Convert.ToDateTime(requests.requestDate);
            int requestStatusId = Convert.ToInt32(requests.requestStatusId);

            foreach (var item in itemList)
            {
                int requestItemId = Convert.ToInt32(item.productId);
                string requestItemName = item.productName;
                int requestItemQNT = Convert.ToInt32(item.requiredQuantity);
                int allocatedQNT = 0;

                using (SqlConnection connectionForItem = new SqlConnection(connectionString))
                {
                    string queryItem = "insert into RequestProductMap values(" + requestId + "," + requestItemId + ",'" + requestItemName + "'," + requestItemQNT + "," + allocatedQNT + ");";

                    SqlCommand commandItem = new SqlCommand(queryItem, connectionForItem);

                    connectionForItem.Open();
                    SqlDataReader sqlReaderItem = commandItem.ExecuteReader();
                    connectionForItem.Close();
                }

            }


            string query = "insert into RequestDetails values(" + requestId + "," + requesterId + "," + requestPriority + ",'" + requestDate + "'," + requestStatusId + ");";

            SqlCommand command = new SqlCommand(query, connection);

            connection.Open();
            SqlDataReader sqlReader = command.ExecuteReader();
            connection.Close();
        }


        public List<RequestProduct> getRequests()
        {

            List<RequestProduct> requestList = new List<RequestProduct>();


            SqlConnection connection = new SqlConnection(connectionString);

            string query = "SELECT * FROM RequestDetails";

            SqlCommand command = new SqlCommand(query, connection);

            connection.Open();
            SqlDataReader sqlReader = command.ExecuteReader();

            while (sqlReader.Read())
            {
                RequestProduct request = new RequestProduct();

                request.requestId = (int)sqlReader.GetValue(sqlReader.GetOrdinal("requestId"));
                request.requesterId = (int)sqlReader.GetValue(sqlReader.GetOrdinal("requesterId"));
                request.requestPriority = (int)sqlReader.GetValue(sqlReader.GetOrdinal("requestPriority"));
                request.requestDate = (DateTime)sqlReader.GetValue(sqlReader.GetOrdinal("requestDate"));
                request.requestStatusId = (int)sqlReader.GetValue(sqlReader.GetOrdinal("requestStatusId"));


                requestList.Add(request);

            }

            connection.Close();
            return requestList;


        }
    }
}