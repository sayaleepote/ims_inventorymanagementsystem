﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfService_Authentication
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IDisplayFinalPurchase" in both code and config file together.
    [ServiceContract]
    public interface IDisplayFinalPurchase
    {
        [OperationContract]
        List<SubOrder> getSubOrders(int requestID, int[] productIDs, int[] purchaseQnts, int[] vendorIDs);

    }

    [DataContract]
    public class Order
    {
        [DataMember]
        public int orderID { get; set; }

        [DataMember]
        public int requestID { get; set; }

        [DataMember]
        public DateTime purchaseDate { get; set; }

        [DataMember]
        public int orderTotal { get; set; }

        [DataMember]
        public int Status { get; set; }
    }

    [DataContract]
    public class SubOrder
    {
        [DataMember]
        public int orderID { get; set; }

        [DataMember]
        public int subOrderID { get; set; }

        [DataMember]
        public int vendorID { get; set; }

        [DataMember]
        public int productID { get; set; }

        [DataMember]
        public int requestID { get; set; }

        [DataMember]
        public int productName { get; set; }

        [DataMember]
        public int purchaseQuantity { get; set; }

        [DataMember]
        public int subOrderTotal { get; set; }

        [DataMember]
        public int Status { get; set; }

        [DataMember]
        public string invoiceLink { get; set; }

        [DataMember]
        public DateTime arrivalDate { get; set; }
    }
}
