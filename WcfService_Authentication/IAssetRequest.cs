﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
using System.Data;

namespace WcfService_Authentication
{
    [ServiceContract]
    public interface IAssetManagement
    {
        [OperationContract]
        List<RequestProduct> getRequests();

        [OperationContract]
        void setRequests(RequestProduct user, List<RequestProductItem> itemList);

    }

    [DataContract]
    public class RequestProduct
    {
        private int requestIdReference;
        private int requesterIdReference;
        private int requestPriorityReference;
        private DateTime requesterDateReference;
        private int requestStatusIdReference;

        public RequestProduct()
        {

        }
        public RequestProduct(int _requestId, int _requesterId, int _requestPriority, DateTime _requestDate, int _requestStatusId)
        {
            // TODO: Complete member initialization
            this.requestIdReference = _requestId;
            this.requesterIdReference = _requesterId;
            this.requestPriorityReference = _requestPriority;
            this.requesterDateReference = _requestDate;
            this.requestStatusIdReference = _requestStatusId;
        }

        [DataMember]
        public int requestId { get; set; }

        [DataMember]
        public int requesterId { get; set; }

        [DataMember]
        public int requestPriority { get; set; }

        [DataMember]
        public DateTime requestDate { get; set; }

        [DataMember]
        public int requestStatusId { get; set; }


    }

    [DataContract]
    public class RequestProductItem
    {
        private int productIdReference;
        private string productNameReference;
        private int requiredQNTReference;

        public RequestProductItem()
        {

        }
        public RequestProductItem(int _pID, string _pName, int _reqQNT)
        {
            // TODO: Complete member initialization
            this.productIdReference = _pID;
            this.productNameReference = _pName;
            this.requiredQNTReference = _reqQNT;
        }

        [DataMember]
        public int productId { get; set; }

        [DataMember]
        public string productName { get; set; }

        [DataMember]
        public int requiredQuantity { get; set; }

    }
}
