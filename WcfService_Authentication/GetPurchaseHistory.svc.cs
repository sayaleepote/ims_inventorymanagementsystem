﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfService_Authentication
{
    
    public class GetPurchaseHistory : IGetPurchaseHistory
    {
        string connectionString = ConfigurationManager.ConnectionStrings
                                   ["DBconnection"].ConnectionString;
        
        public List<PurchaseHistory> getPurchaseHistory()
        {
            List<PurchaseHistory> purchaseList = new List<PurchaseHistory>();


            using (SqlConnection connection = new SqlConnection(connectionString))
            {

               string query = "SELECT * FROM SubOrder";

               connection.Open();
               using( SqlCommand command = new SqlCommand(query, connection))
               { 
                    using(SqlDataReader sqlReader = command.ExecuteReader())
                    { 

                       while (sqlReader.Read())
                       {
                             PurchaseHistory purchase = new PurchaseHistory();
                             purchase.SubOrderID = (int)sqlReader.GetValue(sqlReader.GetOrdinal("SubOrder_ID"));
                             purchase.OrderID = (int)sqlReader.GetValue(sqlReader.GetOrdinal("Order_ID"));
                             purchase.VendorID = (int)sqlReader.GetValue(sqlReader.GetOrdinal("Vendor_ID"));
                             purchase.ProductID = (int)sqlReader.GetValue(sqlReader.GetOrdinal("Product_ID"));
                             purchase.QuantityOrdered = (int)sqlReader.GetValue(sqlReader.GetOrdinal("Quantity_To_Order"));
                             purchase.BillAmount = (int)sqlReader.GetValue(sqlReader.GetOrdinal("Price"));
                            // purchase.Invoice = (string)sqlReader.GetValue(sqlReader.GetOrdinal("Invoice"));
                             purchase.ArrivalDate = Convert.ToDateTime(sqlReader.GetValue(sqlReader.GetOrdinal("ArrivalDate")));

                             purchaseList.Add(purchase);
                     }
                   }
               }
            }
           
            return purchaseList;

        }
    }
}
