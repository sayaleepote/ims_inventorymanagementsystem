﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfService_Authentication
{
    [ServiceContract]
    public interface IGetPurchaseHistory
    {
        [OperationContract]
        List<PurchaseHistory> getPurchaseHistory();
    }

    [DataContract]
    public class PurchaseHistory
    {
        [DataMember]
        public int SubOrderID { get; set; }

        [DataMember]
        public int OrderID { get; set; }

        [DataMember]
        public int VendorID { get; set; }

        [DataMember]
        public int ProductID { get; set; }

        [DataMember]
        public int QuantityOrdered { get; set; }

        [DataMember]
        public int BillAmount { get; set; }

        [DataMember]
        public string Invoice { get; set; }

        [DataMember]
        public DateTime ArrivalDate { get; set; }
    }
}
