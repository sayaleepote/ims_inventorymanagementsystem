﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfService_Authentication
{
   

    public class RequestDetailsRetrieval : IRequestDetailsRetrieval
    {
        string connectionString = ConfigurationManager.ConnectionStrings
                                  ["DBconnection"].ConnectionString;

        public RequestDetail getRequestDetail(int requestId)
        {
            RequestDetail requestDetail = new RequestDetail();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {

                string query = "SELECT * FROM [RequestDetails] where requestId = @requestID";

                connection.Open();
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@requestID", requestId);

                    using (SqlDataReader sqlReader = command.ExecuteReader())
                    {

                        while (sqlReader.Read())
                        {
                            requestDetail.requestId = (int)sqlReader.GetValue(sqlReader.GetOrdinal("requestId"));
                            requestDetail.requesterId = (int)sqlReader.GetValue(sqlReader.GetOrdinal("requesterId"));
                            requestDetail.requestPriority = (int)sqlReader.GetValue(sqlReader.GetOrdinal("requestPriority"));
                            requestDetail.requestDate = Convert.ToDateTime(sqlReader.GetValue(sqlReader.GetOrdinal("requestDate")));
                            requestDetail.requestStatusId = (int)sqlReader.GetValue(sqlReader.GetOrdinal("requestStatusId"));

                        }
                    }
                }
            }

            return requestDetail;
        }


        public List<RequestProductMap> getRequestProductsDetails(int requestId)
        {
            List<RequestProductMap> requestProductList = new List<RequestProductMap>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {

                string query = "SELECT * FROM [RequestProductMap] where requestId = @requestID";
                string queryAvailableQuantity = "SELECT [Available_Quantity] FROM [ProductDetails] where Product_ID = @productID";

                connection.Open();
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@requestID", requestId);

                    

                    using (SqlDataReader sqlReader = command.ExecuteReader())
                    {

                        while (sqlReader.Read())
                        {
                            RequestProductMap requestProduct = new RequestProductMap();

                            requestProduct.requestId = (int)sqlReader.GetValue(sqlReader.GetOrdinal("RequestId"));
                            requestProduct.productId = (int)sqlReader.GetValue(sqlReader.GetOrdinal("ProductId"));
                            requestProduct.productName = (string)sqlReader.GetValue(sqlReader.GetOrdinal("ProductName"));
                            requestProduct.requiredQuantity = (int)sqlReader.GetValue(sqlReader.GetOrdinal("RequiredQnt"));

                            SqlConnection connection2 = new SqlConnection(connectionString);
                            SqlCommand command2 = new SqlCommand(queryAvailableQuantity, connection2);
                            command2.Parameters.AddWithValue("@productID", requestProduct.productId);

                            connection2.Open();

                            SqlDataReader sqlReader2 = command2.ExecuteReader();
                            if (sqlReader2.Read())
                            {
                                requestProduct.availableQuantity = (int)sqlReader2.GetValue(sqlReader2.GetOrdinal("Available_Quantity"));
                            }

                            else
                            {
                                requestProduct.availableQuantity = 0;
                            }

                            requestProductList.Add(requestProduct);

                            sqlReader2.Close();
                            connection2.Close();

                        }
                        
                    }
                }
            }

            return requestProductList;
        }


        public void changeRequestStatusToSent(int requestId)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string query = "UPDATE [RequestDetails] SET requestStatusId = 1 WHERE requestId = @requestID";
           
                connection.Open();
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@requestID", requestId);
                    command.ExecuteNonQuery();
                }
                connection.Close();
            }
        }


        public void changeRequestStatusToGranted(int requestId)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string query = "UPDATE [RequestDetails] SET requestStatusId = 3 WHERE requestId = @requestID";

                connection.Open();
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@requestID", requestId);
                    command.ExecuteNonQuery();
                }
                connection.Close();
            }
        }
    }
}
