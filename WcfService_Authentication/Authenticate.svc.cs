﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfService_Authentication
{
   
    public class Authenticate : IAuthenticate
    {
        string connectionString = ConfigurationManager.ConnectionStrings
                                   ["DBconnection"].ConnectionString;

        public int AuthenticateUser(LoginClass user)
        {
            using(SqlConnection connection = new SqlConnection(connectionString))
            { 
                 string query = "SELECT Authority FROM Login WHERE Login_ID like @username AND Password like @Password";
                 connection.Open();

                 using(SqlCommand command = new SqlCommand(query, connection))
                 { 

                    command.Parameters.AddWithValue("@username", user.userName);
                    command.Parameters.AddWithValue("@Password", user.password);

            
                    using(SqlDataReader sqlReader = command.ExecuteReader())
                    { 
                         if (sqlReader.Read())
                         { 
                             int authority =(int)sqlReader.GetValue(sqlReader.GetOrdinal("Authority"));          
               
                             return authority;
                         }

                         else
                         {
               
                              return -1;
                            //error handle
                         }
                    }
                 }
            }

        }
    }
}
