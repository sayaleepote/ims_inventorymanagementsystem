﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfService_Authentication
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "CreateUserService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select CreateUserService.svc or CreateUserService.svc.cs at the Solution Explorer and start debugging.
    public class CreateUserService : ICreateUserService
    {
        private CreateUser userRef = new CreateUser();

        public int AddNewUser(string _UserName, string _HashedPassword, int _Authority)
        {
            return userRef.AddNewUser(_UserName, _HashedPassword, _Authority);
        }
    }
}
