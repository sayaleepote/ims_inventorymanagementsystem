﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfService_Authentication
{
   
    public class DisplayFinalPurchase : IDisplayFinalPurchase
    {
        string connectionString = ConfigurationManager.ConnectionStrings
                                   ["DBconnection"].ConnectionString;

        public List<SubOrder> getSubOrders(int requestID, int[] productIDs, int[] purchaseQnts, int[] vendorIDs)
        {
            List<SubOrder> subOrderList = new List<SubOrder>();

            List<int> subOrderTotalList = new List<int>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                for (int i = 0; i < productIDs.Length; i++)
                {
                    string query = "SELECT ([Price]) FROM [ProductVendorMap] WHERE [Product_ID] = @productID AND [Vendor_ID] = @vendorID" ;

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {                        
                        command.Parameters.AddWithValue("@vendorID", vendorIDs[i]);
                        command.Parameters.AddWithValue("@productID", productIDs[i]);
                       
                        using (SqlDataReader sqlReader = command.ExecuteReader())
                        {

                            while (sqlReader.Read())
                            {                             
                                int price = (int)sqlReader.GetValue(sqlReader.GetOrdinal("Price"));
                                int subOrderTotal = price * purchaseQnts[i];

                                subOrderTotalList.Add(subOrderTotal);
                            }

                            sqlReader.Close();
                        }
                       
                    }
                }
                
                connection.Close();
            }


            Random randomNumber = new Random();
            int OrderID = randomNumber.Next(100, 1000000);


            using (SqlConnection connection = new SqlConnection(connectionString))
            {

                connection.Open();
                for (int i = 0; i < productIDs.Length; i++)
                {
                    string subOrderString = OrderID.ToString() + i.ToString();
                    int subOrderID = Convert.ToInt32(subOrderString);

                    string query = "INSERT INTO [SubOrder] ([SubOrder_ID],[Order_ID],[Vendor_ID],[Product_ID],[Request_ID],[Quantity_To_Order],[Price],[Status]) values (@subOrderID, @orderID, @vendorID, @productID,@requestID, @purchaseQuantity,@Price, 0)";

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {

                        command.Parameters.AddWithValue("@subOrderID", subOrderID);
                        command.Parameters.AddWithValue("@orderID", OrderID);
                        command.Parameters.AddWithValue("@vendorID",vendorIDs[i]);
                        command.Parameters.AddWithValue("@productID", productIDs[i]);
                        command.Parameters.AddWithValue("@requestID", requestID);
                        command.Parameters.AddWithValue("@purchaseQuantity", purchaseQnts[i]);
                        command.Parameters.AddWithValue("@Price", subOrderTotalList[i]);
                         
                        command.ExecuteNonQuery();                              

                    }

                }

                connection.Close();
            }

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                string query = "INSERT INTO [Order] ([Order_ID],[Request_ID],[Total_Amount],[Approval_Status]) values (@orderID, @requestID, @totalAmount, 0)";
                 int orderTotal = 0;
                 foreach (var price in subOrderTotalList)
                 {
                     orderTotal += price;
                 }

                 connection.Open();
                 using (SqlCommand command = new SqlCommand(query, connection))
                  {
                     command.Parameters.AddWithValue("@orderID", OrderID);
                     command.Parameters.AddWithValue("@requestID", requestID);
                     command.Parameters.AddWithValue("@totalAmount", orderTotal);

                     command.ExecuteNonQuery();
                     
                  }

                  connection.Close();               
            }

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                string query = "SELECT * FROM [SubOrder] WHERE [Request_ID] = @requestID";

                using (SqlCommand command = new SqlCommand(query, connection))
                {
                   command.Parameters.AddWithValue("@requestID", requestID);

                   using (SqlDataReader sqlReader = command.ExecuteReader())
                   {

                      while (sqlReader.Read())
                      {
                          SubOrder subOrderObj = new SubOrder();

                          subOrderObj.subOrderID = (int)sqlReader.GetValue(sqlReader.GetOrdinal("SubOrder_ID"));
                          subOrderObj.orderID = (int)sqlReader.GetValue(sqlReader.GetOrdinal("Order_ID"));
                          subOrderObj.vendorID = (int)sqlReader.GetValue(sqlReader.GetOrdinal("Vendor_ID"));
                          subOrderObj.productID = (int)sqlReader.GetValue(sqlReader.GetOrdinal("Product_ID"));
                          subOrderObj.requestID = (int)sqlReader.GetValue(sqlReader.GetOrdinal("Request_ID"));
                          subOrderObj.purchaseQuantity = (int)sqlReader.GetValue(sqlReader.GetOrdinal("Quantity_To_Order"));
                          subOrderObj.subOrderTotal = (int)sqlReader.GetValue(sqlReader.GetOrdinal("Price"));
                          subOrderObj.Status = (int)sqlReader.GetValue(sqlReader.GetOrdinal("Status"));

                          subOrderList.Add(subOrderObj);
                       }

                      sqlReader.Close();
                    }

                    }


                connection.Close();
            }
            return subOrderList;
        }
    }
}
