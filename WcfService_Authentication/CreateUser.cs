﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Security.Cryptography;

namespace WcfService_Authentication
{
    public class CreateUser : ICreateUserService
    {
        private SqlConnection _connection = null;
        private string _statement = null;
        private SqlCommand _command = null;

        public CreateUser()
        {
            _connection = new SqlConnection(DBConnection.connectionString);
            _statement = "";
            _command = new SqlCommand();
        }

        public int AddNewUser(string _UserName, string _HashedPassword, int _Authority)
        {
            int status = -1;

            _HashedPassword = HashSHA1(_HashedPassword);
            _statement = "insert into Login values ('" + _UserName + "', '" + _HashedPassword + "', " + _Authority + ")";
            _command = new SqlCommand(_statement, _connection);

            _connection.Open();
            status = (_command.ExecuteNonQuery() > 0) ? 0 : 1;
            _connection.Close();

            return status;
        }

        private static string HashSHA1(string value)
        {
            var sha1 = SHA1.Create();
            var inputBytes = Encoding.ASCII.GetBytes(value);
            var hash = sha1.ComputeHash(inputBytes);

            var stringBuilder = new StringBuilder();

            for (var i = 0; i < hash.Length; i++)
            {
                stringBuilder.Append(hash[i].ToString("X2"));
            }

            return stringBuilder.ToString();
        }
    }
}