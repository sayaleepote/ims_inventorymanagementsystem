﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfService_Authentication
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "VendorService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select VendorService.svc or VendorService.svc.cs at the Solution Explorer and start debugging.
    public class VendorService : IVendorService
    {
        Vendor vendor = new Vendor();

        public List<Vendor> GetAllVendors()
        {
            return vendor.GetAllVendors();
        }

        public Vendor GetVendorDetails(int vendorID)
        {
            return vendor.GetVendorDetails(vendorID);
        }

        public int DeleteVendor(int vendorID)
        {
            return vendor.DeleteVendor(vendorID);
        }

        public int UpdateVendor(Vendor vendorRef)
        {
            return vendor.UpdateVendor(vendorRef);
        }

        public int CreateVendor(Vendor vendorRef, List<vendorItem> vendorItemList)
        {
            return vendor.CreateVendor(vendorRef, vendorItemList);
        }

    }
}
